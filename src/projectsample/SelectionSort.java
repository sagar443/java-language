package projectsample;

public class SelectionSort {

    public static void main(String[] args) {

        int arr[] = {5, 3, 4, 1, 2};
        System.out.println("before");
        printArray(arr);
        for (int i = 0; i < arr.length - 1; i++) {
            int lowIndex = i;

            for (int j = i + 1; j < arr.length; j++) {
                if (arr[lowIndex] > arr[j]) {
                    lowIndex = j;
                }

            }
            int temp = arr[i];
            arr[i] = arr[lowIndex];
            arr[lowIndex] = temp;

        }
        System.out.println("after");
        printArray(arr);

    }

    static void printArray(int[] arr) {
        for (int i : arr) {
            System.out.print(i + " ");
        }
    }
}
