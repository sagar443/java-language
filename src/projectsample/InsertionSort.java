package projectsample;

public class InsertionSort {

    public static void sort(int arr[]) {
        int n = arr.length;
        System.out.println("before sorting aray:");
        for (int j = 0; j < n; ++j) {
            System.out.print(arr[j] + " ");
        }
        for (int i = 1; i < n; ++i) {
            int temp = arr[i];
            int j = i - 1;

            while (j >= 0 && arr[j] > temp) {
                arr[j + 1] = arr[j];
                j--;
            }
            arr[j + 1] = temp;
        }
    }

    static void printArray(int arr[]) {
        int n = arr.length;
        System.out.println("after sorting array:");
        for (int i = 0; i < n; ++i) {
            System.out.print(arr[i] + " ");
        }

        System.out.println();
    }

    public static void main(String args[]) {
        int arr[] = {12, 11, 13, 5, 6};
        sort(arr);
        printArray(arr);
    }
}
